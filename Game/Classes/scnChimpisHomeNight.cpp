#include "scnChimpisHomeNight.h"


USING_NS_CC;

Scene* scnChimpisHome_N::createScene(){
    return scnChimpisHome_N::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename){
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in scnChimpisHomeNight.cpp\n");
}

// on "init" you need to initialize your instance
bool scnChimpisHome_N::init(){
    if (!Scene::init())	return false;
    
	cocos2d::Director* director = Director::getInstance();

	_camera = this->getDefaultCamera();
	
	_bg = cocos2d::Sprite::create(BG_FILE);
	_bg->setAnchorPoint(Vec2::ZERO);
	_bg->setPosition(Vec2(0, 0));
	_bg->setScale(1.f);
		
	_akrasiel = cocos2d::Sprite::create(AKRASIEL_FILE);			
	_akrasiel->setPosition(Vec2(getSpriteWidth(_bg) * 0.9f, getSpriteHeight(_bg) * 0.1f));
	_akrasiel->setScale(1.f);
	
	this->addChild(_bg, 0);
	this->addChild(_akrasiel, 1);

	

	_camera->setPosition(Vec2(getSpriteWidth(_bg) * 0.5f, getSpriteHeight(_bg) * 0.5f));
	


	///_chimpis = cocos2d::Sprite::create(CHIMPIS_FILE);				//Jugador





    return true;
}

void scnChimpisHome_N::start_scene(){

}

void scnChimpisHome_N::play_standard_action(const char act){

}

void scnChimpisHome_N::on_press_button(const char btn){

}
