#ifndef SCNCHIMPISHOMENIGHT_H
#define SCNCHIMPISHOMENIGHT_H

#include <string.h>
#include "scnInteractableType.h"

class scnChimpisHome_N : public scnInteractableType{

private:
	
	const std::string BG_FILE{ "BGs/bg_scene00_1024x768.png" };
	const std::string AKRASIEL_FILE{ "Players/pl_akrasiel.png" };
	const std::string CHIMPIS_FILE{ "Players/pl_chimpis.png" };

	cocos2d::Camera* _camera{};
	cocos2d::Sprite* _bg{};
	cocos2d::Sprite* _akrasiel{};
	cocos2d::Sprite* _chimpis{};

	inline float getSpriteWidth(cocos2d::Sprite *& spr) const { return spr->getBoundingBox().size.width; }
	inline float getSpriteHeight(cocos2d::Sprite *& spr) const { return spr->getBoundingBox().size.height; }


protected:

    //Funciones heredadas
    void start_scene() override;
    void play_standard_action(const char act) override;
    void on_press_button(const char btn) override;

public:

    static cocos2d::Scene* createScene();
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(scnChimpisHome_N);
};

#endif // SCNCHIMPISHOMENIGHT_H
