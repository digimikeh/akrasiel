#include "scnMaster.h"

//AudioEngine.h
/*
void scnMaster::bgm_stop_faded(int& index){
    const float source_vol{ cocos2d::AudioEngine::getVolume(index) };
	cocos2d::ActionFloat* af0 = cocos2d::ActionFloat::create(5.0f, source_vol, 0.0f, [&](float vol_val) {
        cocos2d::AudioEngine::setVolume(index, source_vol);
        if (vol_val < 0.05f)	cocos2d::AudioEngine::stop(index);
	});
	runAction(af0);
}

void scnMaster::bgm_fade_volume_to(int& index, const float target_vol) {
    const float source_vol{ cocos2d::AudioEngine::getVolume(index)};
	cocos2d::ActionFloat* af0 = cocos2d::ActionFloat::create(3.0f, source_vol, target_vol, [&](float vol_val) {
        cocos2d::AudioEngine::setVolume(index, vol_val);
	});
	runAction(af0);
}
*/

//SimpleAudioEngine.h is inline



void scnMaster::transition_set(scnMaster::eTransitionMode t) {
	
    if (!_transition){
        CCLOG("Nullptr -> _transition");
        return;
    }
	

	cocos2d::Action* a0 = nullptr;
	_transition->setScale(C_TRANSITION_FULLSCREEN_SCALE);

	switch (t) {

	case eTransitionMode::showVertical:
	{
		_transition->setPosition(C_TRANSITION_TOP_POS);
		a0 = cocos2d::MoveTo::create(C_TRANSITION_TIME, C_TRANSITION_ONSCREEN_POS);
		
	} break;

	case eTransitionMode::showHorizontal:
	{
		_transition->setPosition(C_TRANSITION_RIGHT_POS);
		a0 = cocos2d::MoveTo::create(C_TRANSITION_TIME, cocos2d::Vec2(C_TRANSITION_ONSCREEN_POS));

	} break;

	case eTransitionMode::showFaded:
	{
		_transition->setPosition(C_TRANSITION_ONSCREEN_POS);
		a0 = cocos2d::ActionFloat::create(C_TRANSITION_TIME, 0.0f, 1.0f, [&](float o_val) { _transition->setOpacity(o_val);});

	} break;

	case eTransitionMode::showScaled:
	{
		_transition->setScale(0.0f);
		_transition->setPosition(C_TRANSITION_ONSCREEN_POS);
		a0 = cocos2d::ScaleTo::create(C_TRANSITION_TIME, C_TRANSITION_FULLSCREEN_SCALE);

	} break;

	case eTransitionMode::hideVertical:
	{
		_transition->setPosition(C_TRANSITION_ONSCREEN_POS);
		a0 = cocos2d::MoveTo::create(C_TRANSITION_TIME, cocos2d::Vec2(C_TRANSITION_TOP_POS));

	} break;

	case eTransitionMode::hideHorizontal:
	{
		_transition->setPosition(C_TRANSITION_ONSCREEN_POS);
		a0 = cocos2d::MoveTo::create(C_TRANSITION_TIME, cocos2d::Vec2(C_TRANSITION_RIGHT_POS));

	} break;

	case eTransitionMode::hideFaded:
	{
		_transition->setPosition(C_TRANSITION_ONSCREEN_POS);
		a0 = cocos2d::ActionFloat::create(C_TRANSITION_TIME, 1.0f, 0.0f, [&](float o_val) { _transition->setOpacity(o_val);});

	} break;

	case eTransitionMode::hideScaled:
	{
		_transition->setPosition(C_TRANSITION_ONSCREEN_POS);
		a0 = cocos2d::ScaleTo::create(C_TRANSITION_TIME, 0.0f);
	} break;

	}

	_transition->runAction(a0);
}


