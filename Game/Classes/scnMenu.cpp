#include "scnMenu.h"
#include "AudioEngine.h"
#include "scnPrologue.h"

USING_NS_CC;

Scene* scnMenu::createScene() {
	return scnMenu::create();
}

static void problemLoading(const char* filename) {
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in scnChimpisHomeNight.cpp\n");
}

bool scnMenu::init() {

    if (!Scene::init()) return false;

	//1 - Definiciones de objetos
	_transition = Sprite::create(C_FILE_TRANSICION);

	_bg_stars = cocos2d::Sprite::create(C_FILE_BG);
	_bg_stars->setPosition(C_BG_BEGIN_POS);
	_bg_stars->setScale(0.7f);

	_tittle = cocos2d::Sprite::create(C_FILE_TITTLE);
	_tittle->setPosition(Vec2(0, 0));
	_tittle->setScale(1.f);

	//Integrar los items a un menu
    MenuItemImage* itmStartGame = MenuItemImage::create(C_FILE_BTN_START_NORMAL, C_FILE_BTN_START_PRESSED, [&](Ref* sender) { on_press_button(static_cast<const char>(eButtons::start_game)); });
    MenuItemImage* itmOptions = MenuItemImage::create(C_FILE_BTN_OPTIONS_NORMAL, C_FILE_BTN_OPTIONS_PRESSED, [&](Ref* pSender){ on_press_button(static_cast<const char>(eButtons::options));  });
    MenuItemImage* itmExit = MenuItemImage::create(C_FILE_BTN_EXIT_NORMAL, C_FILE_BTN_EXIT_PRESSED, [&](Ref* pSender) { on_press_button(static_cast<const char>(eButtons::exit)); });

    ui_buttons.pushBack(itmStartGame);
    ui_buttons.pushBack(itmOptions);
    ui_buttons.pushBack(itmExit);

    Menu* _main_menu = Menu::createWithArray(ui_buttons);
	
	//2 - Adjuntamientos
	this->addChild(_bg_stars, 0);
	this->addChild(_tittle, 1);
	this->addChild(_main_menu, 1);
	this->addChild(_transition, 9);

	//3 - Configuraciones iniciales
	this->getDefaultCamera()->setPosition(cocos2d::Vec2(0, 0));
	_main_menu->setPosition(Vec2(0, -40));
	_transition->setPosition(Vec2::ZERO);	// <-+ transition siempre esta centrado y tapa la pantalla al iniciar escena
    //_transition->setScale(C_TRANSITION_FULLSCREEN_SCALE);				// <-|
	
	short int p = 0;

    for (MenuItem* i : ui_buttons) {
		if (i) {
			i->setScale(.5f);
			i->setPosition(Vec2(0, p));
			p -= 30;
		}		
	}
		
	//4 - Inicio
    start_scene();

	return true;
}


void scnMenu::start_scene() {
	play_standard_action(static_cast<const char>(eStandard_actions::cmpStart_scene));
}

///ALERTA: Evitar recursividad aqui
///ALERTA 2: Cuidado con las acciones que se repiten por siempre, no incluir eventos que no tengan
///				que repetirse, ejemplo: play bgm...
void scnMenu::play_standard_action(const char act) {
	switch (static_cast<eStandard_actions>(act)) {

	case eStandard_actions::cmpStart_scene:
	{
        //bgm_menu = bgm_play(C_FILE_BGM, true);
        bgm_play(C_FILE_BGM, true);
		//mueve fondo estrellas (loop)
		MoveTo* a0 = MoveTo::create(C_BG_ANIM_DELAY_TIME, C_BG_END_POS);
		CallFunc* a1 = CallFunc::create([&]() { _bg_stars->setPosition(C_BG_BEGIN_POS); });
		Sequence* a_exe = Sequence::create(a0, a1, nullptr);
		_bg_stars->runAction(RepeatForever::create(a_exe));

        //entinta el logo y titulo (loop)
		TintTo* b0 = TintTo::create(1.f, Color3B::GREEN);
		TintTo* b1 = TintTo::create(1.f, Color3B::MAGENTA);
		TintTo* b2 = TintTo::create(1.f, Color3B::GRAY);
		TintTo* b3 = TintTo::create(1.f, Color3B::ORANGE);
		Sequence* b_exe = Sequence::create(b0, b1, b2, b3, nullptr);
		_tittle->runAction(RepeatForever::create(b_exe));

		//transicion de apertura
		transition_set(eTransitionMode::hideScaled);
		

	} break;

	case eStandard_actions::btnStart_game:
	{
        //bgm_fade_volume_to(bgm_menu, 0.0f);
        bgm_stop();
		sfx_play(C_FILE_SFX_START);
		transition_set(eTransitionMode::showScaled);
		this->schedule([&](float dt) {
			cocos2d::Scene* scn = scnPrologue::create();
			Director::getInstance()->replaceScene(scn);
		}, 0.0f, 0, C_TRANSITION_TIME + 5.0f, "schedulekey");
		

	} break;

	case eStandard_actions::btnExit:
	{
        //bgm_fade_volume_to(bgm_menu, 0.0f);
        bgm_stop();
		sfx_play(C_FILE_SFX_END);
		transition_set(eTransitionMode::showVertical);
        log("hola");
		this->schedule([&](float dt) { Director::getInstance()->end(); }, 0.0f, 0, C_TRANSITION_TIME + 5.0f, "schedulekey");
	}
	}//fin switch-case
}

void scnMenu::on_press_button(const char btn) {

	if (!button_lock) {

		button_lock = true;

		switch (static_cast<eButtons>(btn)) {
		case eButtons::start_game:
			play_standard_action(static_cast<const char>(eStandard_actions::btnStart_game));
			break;

		case eButtons::options:
			log("Options");
			break;

		case eButtons::exit:
			play_standard_action(static_cast<const char>(eStandard_actions::btnExit));
			break;
		}
	}

	
}


