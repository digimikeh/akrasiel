#ifndef SCNMASTER_H
#define SCNMASTER_H

#pragma once
#include <cocos2d.h>
//#include "AudioEngine.h"
#include "editor-support/cocostudio/SimpleAudioEngine.h"

#include <map>

class scnMaster : public cocos2d::Scene {
	   
private:
    const float				C_TRANSITION_FULLSCREEN_SCALE	= 7.8f;
    const cocos2d::Vec2		C_TRANSITION_TOP_POS			= cocos2d::Vec2(0.0f, 325.0f);
    const cocos2d::Vec2		C_TRANSITION_ONSCREEN_POS		= cocos2d::Vec2(0.0f, 0.0f);
    const cocos2d::Vec2		C_TRANSITION_RIGHT_POS			= cocos2d::Vec2(0.0f, 0.0f);

protected:

	enum class eTransitionMode : char{
		showScaled = '0',
		showHorizontal = '1',
		showVertical = '2',
		showFaded = '3',
		hideScaled = '4',
		hideHorizontal = '5',
		hideVertical = '6',
		hideFaded = '7'
	};

    const char*				C_FILE_TRANSICION			= "Images/Shared/spr_transition.jpg";
    const float				C_TRANSITION_TIME        		= 0.4f;

    cocos2d::Sprite* _transition = nullptr;
    std::map<int, std::string> bgm_tracks{};

    //otra cosa importante es ver si los punteros se est�n liberando al cargar una escena !

	virtual void start_scene() = 0;
	virtual void play_standard_action(const char act) = 0;


	inline void write_text_on_label(cocos2d::Label*& et, const char* txt, cocos2d::Color3B col) {
		et->setColor(col);
		et->setString(txt);
	}

    //AudioEngine.h
    /*
    inline int sfx_play(const char* sfx_file, float vol = 1.f){
        return cocos2d::AudioEngine::play2d(sfx_file, false, vol);
	}

    inline int bgm_play(const char* bgm_file, const bool loop = true) {
        return cocos2d::AudioEngine::play2d(bgm_file, loop);
	}
	
    inline void bgm_stop(int& index) {
        cocos2d::AudioEngine::stop(index);
	}

    void bgm_stop_faded(int& index);
    void bgm_fade_volume_to(int& index, const float target_vol);
    */

    //SimpleAudioEngine.h
    inline void sfx_play(const char* sfx_file){
        CocosDenshion::SimpleAudioEngine* a_engine = CocosDenshion::SimpleAudioEngine::getInstance();
        a_engine->playEffect(sfx_file);
    }

    inline void bgm_play(const char* bgm_file, const bool _loop){
        CocosDenshion::SimpleAudioEngine* a_engine = CocosDenshion::SimpleAudioEngine::getInstance();
        a_engine->playBackgroundMusic(bgm_file, _loop);
    }

    inline void bgm_stop(){
        CocosDenshion::SimpleAudioEngine* a_engine = CocosDenshion::SimpleAudioEngine::getInstance();
        a_engine->stopBackgroundMusic();
    }


	void transition_set(eTransitionMode t);

};

#endif	
