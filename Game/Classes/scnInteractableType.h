#ifndef SCNINTERACTABLETYPE_H
#define SCNINTERACTABLETYPE_H

#pragma once
#include "scnMaster.h"

class scnInteractableType : public scnMaster{
private:

protected:
    bool button_lock{ false };
    cocos2d::Vector<cocos2d::MenuItem*> ui_buttons{};

    virtual void on_press_button(const char mb) = 0;


public:


};

#endif //SCNINTERACTABLETYPE_H

