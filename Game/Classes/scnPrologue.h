//Prologo:

#ifndef SCNPROLOGUE_H
#define SCNPROLOGUE_H

#pragma once
#include "scnInteractableType.h"

class scnPrologue : public scnInteractableType {

private:

	enum class eStandard_actions : char {
		spiral_rotation = '0',
		stripe_next_shot = '1',
		sleep_to_chimpisHome = '2',
		finish_to_chimpisHome = '3'
	};

	enum class eNext_scenes : char {
		chimpis_home = '0'
	};

	enum class eButtons : char {
		next = '0',
		skip = '1'
	};

	//Constantes : Presets

	const char* C_FILE_GB						= "Images/Shared/bg_cinematic.jpg";
	const char* C_FILE_SHOT0					= "Images/Prologue/img_shot0.png";
	const char* C_FILE_SHOT1					= "Images/Prologue/img_shot1.png";
	const char* C_FILE_SHOT2					= "Images/Prologue/img_shot2.png";
	const char* C_FILE_SHOT3					= "Images/Prologue/img_shot3.png";
	const char* C_FILE_BGM						= "Sounds/BGM/bgm_contact.mp3";
	const char* C_FILE_TXTFONT					= "Fonts/fnt_space2.ttf";
	const char* C_FILE_SFXSEQ					= "Sounds/SFX/sfx_seq0.wav";
	const char* C_FILE_SFXLIGHTFALL				= "Sounds/SFX/sfx_lightFall.wav";
	const char* C_FILE_SFXSKIP					= "Sounds/SFX/sfx_10.wav";
	const char* C_FILE_BTN_CTRL_NORMAL			= "Images/Shared/btn_triangle_normal.png";
	const char* C_FILE_BTN_CTRL_PRESSED			= "Images/Shared/btn_triangle_pressed.png";

	const short int C_SHOTS_DISTANCE_FACTOR		= 350;
	const float C_SHOTS_ANIM_TIME				= 0.15f;
	const float C_BG_ANIM_TIME					= 5.0f;
	const short int C_BG_TGT_ANGLE_FACTOR		= 180;

	const char* C_COMMENT_SHOT0					= "This is Chimpis's home in middle \n of a great forest...";
	const char* C_COMMENT_SHOT1					= "...this is Chimpis, a very rare guy...";
	const char* C_COMMENT_SHOT2					= "...that night something shiny was drop into the lake...";
	const char* C_COMMENT_SHOT3					= "Chimpis : what was that ??. \n Let's go check out!.";

	const cocos2d::Vec2 C_NEXT_BTN_POS{ cocos2d::Vec2(170.0f, 90.0f) };
	const cocos2d::Vec2 C_SKIP_BTN_POS{ cocos2d::Vec2(170.0f, 40.0f) };
	const cocos2d::Vec2 C_COMMENTS_POS{ cocos2d::Vec2(0.0f, -150.0f) };

	//Elementos
	cocos2d::Label* comments = nullptr;
	cocos2d::Node* _shotStripe = nullptr;
	cocos2d::Sprite* _bg = nullptr;

    //BGM & SFX IDs
    int bgm_main = 0;
    int sfx_next = 0;
    int sfx_skip = 0;

	//Propiedades
	float nextPos{ 0 };
	bool bButtonsEnabled = false;
	unsigned short int currentShot = 0;

	void start_scene() override;
	void play_standard_action(const char act) override;
    void on_press_button(const char btn) override;


public:

	static cocos2d::Scene* createScene();
	virtual bool init();

	// implement the "static create()" method manually
	CREATE_FUNC(scnPrologue);
};

#endif
