#include "scnPrologue.h"

//#include "scnHomeChimpis.h"

USING_NS_CC;

Scene* scnPrologue::createScene() {
	return scnPrologue::create();
}


static void problemLoading(const char* filename) {
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in scnPrologue.cpp\n");
}


bool scnPrologue::init() {
	if (!Scene::init())	return false;

	//1 - Definitions
    _transition = Sprite::create(C_FILE_TRANSICION);

    MenuItemImage* itmNext = MenuItemImage::create(C_FILE_BTN_CTRL_NORMAL, C_FILE_BTN_CTRL_PRESSED, [&](Ref* pSender) { on_press_button(static_cast<const char>(eButtons::next)); });
    MenuItemImage* itmSkip = MenuItemImage::create(C_FILE_BTN_CTRL_NORMAL, C_FILE_BTN_CTRL_PRESSED, [&](Ref* pSender) { on_press_button(static_cast<const char>(eButtons::skip)); });
    ui_buttons.pushBack(itmNext);
    ui_buttons.pushBack(itmSkip);
    Menu* controlMenu = Menu::createWithArray(ui_buttons);
	comments = Label::createWithTTF("", C_FILE_TXTFONT, 24);
	
	_shotStripe = Node::create();

	cocos2d::Sprite* shot0 = cocos2d::Sprite::create(C_FILE_SHOT0);
	cocos2d::Sprite* shot1 = cocos2d::Sprite::create(C_FILE_SHOT1);
	cocos2d::Sprite* shot2 = cocos2d::Sprite::create(C_FILE_SHOT2);
	cocos2d::Sprite* shot3 = cocos2d::Sprite::create(C_FILE_SHOT3);
	
	cocos2d::Vector<cocos2d::Sprite*> vsprites{};
	vsprites.pushBack(shot0);
	vsprites.pushBack(shot1);
	vsprites.pushBack(shot2);
	vsprites.pushBack(shot3);

	float current_pos = 0.0f;
	for (auto& s : vsprites) {
		s->setPosition(Vec2(0.0f, current_pos));
		s->setScale(0.7f);
		current_pos += C_SHOTS_DISTANCE_FACTOR;
	}

	_bg = cocos2d::Sprite::create(C_FILE_GB);

	//2 - Attachment
	this->addChild(_shotStripe, 1);
	this->addChild(controlMenu, 2);
	this->addChild(comments, 2);
	_shotStripe->addChild(shot0, 1);
	_shotStripe->addChild(shot1, 1);
	_shotStripe->addChild(shot2, 1);
	_shotStripe->addChild(shot3, 1);
	this->addChild(_bg, 0);
    this->addChild(_transition, 9);


	//3 - Configuraciones iniciales
    _transition->setPosition(cocos2d::Vec2(0, -30));
	this->getDefaultCamera()->setPosition(cocos2d::Vec2(0, -30));
	controlMenu->setPosition(Vec2::ZERO);
	itmNext->setAnchorPoint(Vec2(.5f, .5f));
	itmSkip->setAnchorPoint(Vec2(.5f, .5f));
	itmNext->setPosition(C_NEXT_BTN_POS);
	itmSkip->setPosition(C_SKIP_BTN_POS);
	itmNext->setScale(.3f);
	itmSkip->setScale(.3f);
	itmNext->setRotation(-90.0f);
	comments->setPosition(C_COMMENTS_POS);
	
	_bg->setPosition(Vec2::ZERO);
	_bg->setScale(2.f);
	bButtonsEnabled = true;

	//4 - Inicio
	start_scene();
	return true;
}


//Inicia la escena
void scnPrologue::start_scene() {
    bgm_play(C_FILE_BGM, true);
    transition_set(eTransitionMode::hideScaled);        //Not working
	write_text_on_label(comments, C_COMMENT_SHOT0, Color3B::GRAY);
	play_standard_action(static_cast<const char>(eStandard_actions::spiral_rotation));
}

void scnPrologue::play_standard_action(const char act) {
	switch (static_cast<eStandard_actions>(act)) {
	case eStandard_actions::spiral_rotation:
	{
		RotateBy* accion_rot = RotateBy::create(C_BG_ANIM_TIME, C_BG_TGT_ANGLE_FACTOR);
		_bg->runAction(RepeatForever::create(accion_rot));
	}
	break;

	case eStandard_actions::stripe_next_shot:
	{
		++currentShot;

		switch (currentShot) {
		case 1:
			write_text_on_label(comments, C_COMMENT_SHOT1, Color3B::GRAY);
			break;
		case 2:
			sfx_play(C_FILE_SFXLIGHTFALL);
			write_text_on_label(comments, C_COMMENT_SHOT2, Color3B::GRAY);
			break;
		case 3:
			write_text_on_label(comments, C_COMMENT_SHOT3, Color3B::ORANGE);
			break;
		}

		sfx_play(C_FILE_SFXSEQ);
		float topPosition = (C_SHOTS_DISTANCE_FACTOR * (_shotStripe->getChildrenCount() - 1)) * -1;
		nextPos -= C_SHOTS_DISTANCE_FACTOR;
        for (auto& i : ui_buttons)	i->setColor(Color3B(50, 50, 50));
		cocos2d::MoveTo* move_action = MoveTo::create(C_SHOTS_ANIM_TIME, Vec2(0.0f, nextPos));
		CallFunc* function_action = CallFunc::create([&]() {

            ui_buttons.at(0)->setColor(Color3B(255, 255, 200));
            ui_buttons.at(1)->setColor(Color3B(255, 255, 200));
			bButtonsEnabled = true;

		});
        if (nextPos == topPosition)	ui_buttons.at(0)->setVisible(false);
		_shotStripe->runAction(Sequence::create(move_action, function_action, nullptr));
	}
	break;

	case eStandard_actions::finish_to_chimpisHome:
	{
        MoveBy* accion_mov = MoveBy::create(0.1f, Vec2(1000.f, 0.f));
		CallFunc* function_action = CallFunc::create([&]() {
			sfx_play(C_FILE_SFXSKIP);
            for (cocos2d::MenuItem* v : ui_buttons)	v->setEnabled(false);
			

		});

		Sequence* seq = Sequence::create(accion_mov, function_action, nullptr);
		this->getDefaultCamera()->runAction(seq);
	}
	break;
	}
}

void scnPrologue::on_press_button(const char btn){
	switch (static_cast<eButtons>(btn))
	{
	case eButtons::next:
	{
		if (bButtonsEnabled) {
			bButtonsEnabled = false;
			play_standard_action(static_cast<const char>(eStandard_actions::stripe_next_shot));
		}
	}
	break;

	case eButtons::skip:
	{
		if (bButtonsEnabled)	play_standard_action(static_cast<const char>(eStandard_actions::finish_to_chimpisHome));
	}
	break;

	default:
		break;
	}
}
