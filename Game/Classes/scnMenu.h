#ifndef SCNMENU_H
#define SCNMENU_H

#pragma once
#include "scnInteractableType.h"

class scnMenu : public scnInteractableType {

private:	 

	///cmp = composed (acciones compuestas por mas de un sprite)
	///btn = onButton (acciones gatilladas por botones)
	///otro tipo de accionoes no tienen prefijo
	enum class eStandard_actions : char{
		cmpStart_scene = '0',		
		btnStart_game = '1',
		btnExit = '2'
	};

	enum class eNext_scenes : char {
		prologue = '0'
	};

	enum class eButtons : char{
		start_game = '0',
		options = '1',
		exit = '2'
	};

	//Constantes : Presets
	const char*					C_FILE_BG					= "Images/MainMenu/bg_stars.jpg";
	const char*					C_FILE_TITTLE				= "Images/MainMenu/spr_tittle.png";
	const char*					C_FILE_BGM					= "Sounds/BGM/bgm_menu.mp3";		
	const char*					C_FILE_SFX_START			= "Sounds/SFX/sfx_startGame.ogg";
	const char*					C_FILE_SFX_END				= "Sounds/SFX/sfx_endGame.ogg";
	const char*					C_FILE_BTN_START_NORMAL		= "Images/MainMenu/btn_startGame_normal.png";
	const char*					C_FILE_BTN_START_PRESSED	= "Images/MainMenu/btn_startGame_pressed.png";
	const char*					C_FILE_BTN_OPTIONS_NORMAL	= "Images/MainMenu/btn_options_normal.png";
	const char*					C_FILE_BTN_OPTIONS_PRESSED	= "Images/MainMenu/btn_options_pressed.png";
	const char*					C_FILE_BTN_EXIT_NORMAL		= "Images/MainMenu/btn_exit_normal.png";
	const char*					C_FILE_BTN_EXIT_PRESSED		= "Images/MainMenu/btn_exit_pressed.png";
	const float					C_BG_ANIM_DELAY_TIME		= 5.f;

	const cocos2d::Vec2			C_BG_BEGIN_POS				= cocos2d::Vec2(460, 0);
	const cocos2d::Vec2			C_BG_END_POS				= cocos2d::Vec2(-400, 0);

	//Elementos
    cocos2d::Sprite* _bg_stars = nullptr;
	cocos2d::Sprite* _tittle = nullptr;

    //BGM & SFX IDs
    int bgm_menu = 0;
    int sfx_startGame = 0;
    int sfx_endGame = 0;

    //Funciones heredadas
	void start_scene() override;
	void play_standard_action(const char act) override;
    void on_press_button(const char btn) override;
	
public:

	static cocos2d::Scene* createScene();
	virtual bool init() override;

    CREATE_FUNC(scnMenu)
	
};

#endif
